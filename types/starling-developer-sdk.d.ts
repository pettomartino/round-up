import { AxiosResponse } from 'axios'

export declare interface StartlingResponse  {
  data: any
}

export declare interface IBalance {
  clearedBalance: number
  effectiveBalance: number
  pendingTransactions: number
  availableToSpend: number
  acceptedOverdraft: number
  currency: string
  amount: number
}

type AccountResponse = AxiosResponse<IAccount>
type CustomerResponse = AxiosResponse<ICustomer>
type AddressResponse = AxiosResponse<IAddress>
type CardResponse = AxiosResponse<ICard>
type TransactionsResponse = AxiosResponse<Array<ITransaction>>
type PaymentResponse = AxiosResponse<IPayment>
type MandateResponse = AxiosResponse<IMandate>
type ContactResponse = AxiosResponse<IContact>
type SavingsGoalsResponse = AxiosResponse<ISavingsGoals>
type BalanceResponse = AxiosResponse<IBalance>

export declare interface StarlingOptions {
  apiUrl?: string
  oauthUrl?: string
  clientId?: string
  clientSecret?: string
}

declare interface IWhoAmI {}
declare interface ICustomer {}
declare interface IAccount {
  getAccount(accessToken: string):  Promise<AccountResponse>
  getBalance(accessToken: string): Promise<StartlingResponse>

}
declare interface IAddress {}
declare interface ITransaction {
  readonly id: string
  readonly currency: string
  readonly amount: number
  readonly direction: string
  readonly created: string
  readonly narrative: string
  readonly source: string
  readonly balance: number
}

declare interface IPayment {}
declare interface IMandate {}
declare interface IContact {}
declare interface ICard {}
declare interface ISavingsGoals {}
declare interface IOAuth {}
declare interface IWhoAmI {}

declare class WhoAmI implements IWhoAmI {}

declare class Customer implements ICustomer {}

declare class Account implements IAccount {
  getAccount(accessToken: string):  Promise<AccountResponse>
  getBalance(accessToken: string): Promise<StartlingResponse>
}

declare class Address implements IAddress {
  constructor(options: StarlingOptions)
  getAddresses(accessToken: string): Promise<AddressResponse>
}

declare class Card implements ICard {
  /**
   * Creates an instance of the client's card
   * @param {StarlingOptions} options - configuration parameters
   */
  constructor(options: StarlingOptions)
  /**
   * Retrieves a customer's card
   * @param {string} accessToken - the oauth bearer token.
   * @return {Promise} - the http request promise
   */
  getCard (accessToken: string): Promise<AxiosResponse<ICard>>
}

declare class Transaction implements ITransaction {
  constructor(options: StarlingOptions)
  readonly id: string
  readonly currency: string
  readonly amount: number
  readonly direction: string
  readonly created: string
  readonly narrative: string
  readonly source: string
  readonly balance: number
}

declare class Payment implements IPayment {}

declare class Mandate implements IMandate {}

declare class Contact implements IContact {}


declare class SavingsGoals implements ISavingsGoals {}

declare class OAuth implements IOAuth {}

declare enum AccountType {
  UK_ACCOUNT_AND_SORT_CODE = 'UK_ACCOUNT_AND_SORT_CODE',
}

declare class Starling {
    /**
   * Create an instance of the starling client
   * @param {StarlingOptions} options - configuration parameters
   */
  constructor(options: StarlingOptions)

  readonly whoAmI: WhoAmI
  readonly customer: Customer
  readonly account: Account
  readonly address: Address
  readonly transaction: ITransaction
  readonly payment: Payment
  readonly mandate: Mandate
  readonly contact: Contact
  readonly card: Card
  readonly savingsGoals: SavingsGoals
  readonly oAuth: OAuth

  /**
   * Gets the customer UUID and permissions corresponding to the access token passed
   * @param {string} accessToken - the oauth bearer token.  If not
   * specified, the accessToken on the options object is used.
   * @return {Promise<CustomerResponse>}
   */
  getMe(accessToken?: string): Promise<CustomerResponse>


    /**
   * Gets the customer's details
   * @param {string=} accessToken - the oauth bearer token. If not
   * specified, the accessToken on the options object is used.
   * @return {Promise<CustomerResponse>} - the http request promise
   */
  getCustomer(accessToken?: string): Promise<CustomerResponse>

  createContact(
    accessToken: string,
    name: string,
    accountType: AccountType,
    accountNumber: string,
    sortCode: string,
    customerId: string,
  ): Promise<AccountResponse>

  deleteContact(accessToken: string, contactId: string): any

  createSavingsGoal(
    accessToken: string,
    savingsGoalId: string,
    name: string,
    currency: string,
    targetAmount: number,
    targetCurrency: string,
    base64EncodedPhoto: string,
  ): Promise<SavingsGoalsResponse>

  deleteMandate(
    accessToken: string,
    mandateId: string,
  ): Promise<MandateResponse>

  deleteSavingsGoal(
    accessToken: string,
    savingsGoalId: string,
  ): Promise<SavingsGoalsResponse>

  getAccessToken(authorizationCode: string): Promise<StartlingResponse>
  getAccount(accessToken?: string): Promise<StartlingResponse>
  getAddresses(accessToken?: string): Promise<StartlingResponse>
  getBalance(accessToken?: string): Promise<BalanceResponse>
  getCard(accessToken?: string): Promise<StartlingResponse>
  getContactAccount(): Promise<StartlingResponse>
  getContacts(): Promise<StartlingResponse>


  getSavingsGoal(): Promise<StartlingResponse>
  getTransaction(): Promise<StartlingResponse>
  getTransactions(
    accessToken?: string,
    fromDate?: string,
    toDate?: string,
    source?: string,
  ): Promise<TransactionsResponse>
  listMandates(): Promise<StartlingResponse>
  listSavingsGoals(): Promise<AxiosResponse<Array<SavingsGoalsResponse>>>
  listScheduledPayments(): Promise<StartlingResponse>
  makeLocalPayment(): Promise<StartlingResponse>
  refreshAccessToken(): Promise<StartlingResponse>
  addMoneyToSavingsGoal(
    accessToken: string | undefined,
    savingsGoalId: string,
    transactionId: string,
    amount: number,
    currency?: string,
  ): Promise<StartlingResponse>
}

declare interface WebhookContent {
  amount: number
  class: string,
  transactionUid: string,
  sourceCurrency: string,
  sourceAmount: number,
  counterParty: string,
  merchantUid: string,
  merchantLocationUid: string,
  eventUid: string,
  categoryUid: string,
  type: string,
  forCustomer: string,
}


export default Starling
