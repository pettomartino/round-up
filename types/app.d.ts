interface INotifier {
  bellowZero(balance: number): void
}

interface ISender {
  send(emailData: any): Promise<any>
}