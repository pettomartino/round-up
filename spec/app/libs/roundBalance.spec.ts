import 'mocha';
import { expect } from 'chai';

import roundBalance from '../../../app/libs/round'

describe('Round balance', () => {

  it('should round big numbers', () => {
    const valueToRound = roundBalance(560.1)

    expect(valueToRound).to.equal(10);
  })

  it('should round balance below .5', () => {
    const valueToRound = roundBalance(12.1)

    expect(valueToRound).to.equal(10);
  })

  it('should round balance above .5', () => {
    const valueToRound = roundBalance(12.9)

    expect(valueToRound).to.equal(90);
  })

  it('should not round balance of exact number', () => {
    const valueToRound = roundBalance(9)

    expect(valueToRound).to.equal(0);
  })
})