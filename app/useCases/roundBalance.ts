import Starling, { IBalance } from 'starling-developer-sdk';

import guid from '../libs/guid'
import getAmountToRound from '../libs/round'


function roundUseCase(notifier: INotifier, starling: Starling, goalUid: string) {
  async function handleAmountBellowZero(balance: IBalance) {
    await notifier.bellowZero(balance.effectiveBalance)
  }

  async function submitRoundingToSavings(amountToRound: number) {
    await starling.addMoneyToSavingsGoal(
      undefined,
      goalUid,
      guid(),
      amountToRound
    )
  }

  return {
    roundBalance: async () => {
      try {
        let start = new Date().getTime()
        const { data } = await starling.getBalance()
        console.log('request balance: ', new Date().getTime() - start)

        const effectiveBalance = data.effectiveBalance
        const amountToRound = getAmountToRound(effectiveBalance)

        if (effectiveBalance < 0) {
          await handleAmountBellowZero(data)
          return {
            balance: data,
            rounding: 0
          }
        }

        if (amountToRound == 0) {
          return {
            balance: data,
            rounding: 0
          }
        }

        start = new Date().getTime()
        await submitRoundingToSavings(amountToRound)

        console.log('rouding: ', new Date().getTime() - start)

        return {
          balance: data,
          rounding: amountToRound
        }

      } catch(e) {
        return {
          error: e
        }
      }
    }
  }
}


export default roundUseCase

