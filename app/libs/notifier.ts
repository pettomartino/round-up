function notifier(sender: ISender) : INotifier {
  return {
    bellowZero: (balance: number): Promise<any> => {
      const emailData = {
        'FromEmail': 'pettomartino@gmail.com',
        'FromName': 'Petto',
        'Subject': '[Starling Alert] Balance below 0 ',
        'Text-part': `Your balance is now ${balance}`,
        'Recipients': [{'Email': 'pettomartino@gmail.com'}],
      }

      return sender.send(emailData)
    }
  }
}

export default notifier