
/**
 * Gaussian rounding(rounds a number to the nearest even number)
 * Here's the reason to do that: http://www.xbeat.net/vbspeed/i_BankersRounding.htm
 * @param {number} num Number to be rounded
 */
function evenRound(num: number): number {
  const n = +(num).toFixed(8)
  const i = Math.floor(n)
  const f = n - i
  const e = 1e-8
  const r = f > 0.5 - e && f < 0.5 + e ? (i % 2 == 0 ? i : i + 1) : Math.round(n)
  return r
}

function getAmountToRound(amount: number): number {
  return evenRound(amount * 100) - Math.floor(amount) * 100
}

export default getAmountToRound