import mailjet from 'node-mailjet'
import Starling, { StarlingOptions } from 'starling-developer-sdk'
import notifier from './app/libs/notifier'
import roundUseCase from './app/useCases/roundBalance'

const mailer = mailjet
  .connect(
    process.env.MAILJET_KEY,
    process.env.MAILJET_SECRET
  ).post('send')

const accessToken = process.env.STARLING_ACCESS_KEY
const goalUID = process.env.GOAL_UID || ''
const starling = new Starling({ accessToken } as StarlingOptions)

export async function round(event, context, callback) {
  try {
    const usecase = roundUseCase(notifier(mailer), starling, goalUID)
    const rounded = await usecase.roundBalance()

    callback(null, {
      statusCode: 200,
      body: JSON.stringify(rounded),
    })
  } catch(e) {
    callback(null, {
      statusCode: 500,
      body: JSON.stringify(e)
    })
  }
}
